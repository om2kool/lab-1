<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<form method="get">
		Genre:<input type="text" name="genre"></input>
		<input type="submit" name="Go"></input>
	</form>
	
	<%@ page import="java.sql.*"  %>
	<%@ page import="javax.sql.*"  %>
	<%@ page import="javax.naming.*"  %>
	<%@ page import="org.hsqldb.jdbc.JDBCDataSource" %>	
	
	<p>	
	
	<%
		JDBCDataSource ds = new JDBCDataSource();
		
		// setup URL according to HSQLDB specs
		ds.setUrl("jdbc:hsqldb:hsql://localhost/");
		
		// set other data source properties
		ds.setPassword("");
		ds.setUser("SA");
			
		// setup connection and query
		Connection conn = ds.getConnection();
		Statement statement1 = conn.createStatement();		
		
		String querySQL = "select * from book";
		
		String genre = request.getParameter("genre");
		
		//out.println("genre"+genre);
		if (genre==""){
			// send query to database
			querySQL = querySQL;
		} else {
			querySQL = querySQL + " where genre ='"+genre+"'";
		}		
		ResultSet rs = statement1.executeQuery(querySQL);
		ResultSetMetaData rsmd = rs.getMetaData();		
		
		%>
		<table border="1">
		<%
			      	
		// iterate through the results
		while(rs.next())
		{
			%>
			<tr>
				<%
					for(int i=1 ; i<=rsmd.getColumnCount(); i++)
					{
						%>
						<td>
						<%
						out.println(rs.getString(i));
						%>
						</td>
						<%
					}
				%>
			</tr>
			<%
		}		
		%>
		</table>		
		<%		
		
		// close the database resources
		rs.close();		
		statement1.close();		
		conn.close();
	%>		
	
</body>
</html>